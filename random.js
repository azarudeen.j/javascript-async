
function fetchRandomNumbers() {
    return new Promise((resolve, reject) => {
        console.log('Fetching number...');
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString() {
    return new Promise((resolve, reject) => {
        console.log('Fetching string...');
        setTimeout(() => {
            let result = '';
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for (let i = 0; i < 5; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            console.log('Received random string:', result);
            resolve(result);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}


async function task1() {
    try {
        const integerValue = await fetchRandomNumbers();
        console.log(integerValue)
        const stringValue = await fetchRandomString();
        console.log(stringValue)
    } catch (error) {
        console.error(error);
    }
}
task1();

async function task2() {
    try {
        const integerValue1 = await fetchRandomNumbers();
        let sum = integerValue1;
        console.log(sum);
        const integerValue2 = await fetchRandomNumbers();
        sum += integerValue2
        console.log(sum)
    } catch (error) {
        console.error(error);
    }
}
task2();

async function task3() {
    try {
        const integerValue = await fetchRandomNumbers();
        const stringValue = await fetchRandomString();
        const resultValue = integerValue + stringValue;
        console.log(resultValue)
    } catch (error) {
        console.error(error);
    }   
}
task3();

async function task4() {
    try {
        let sum = 0;
        for (let index = 1; index <= 10; index++) {
            let integerValue1 = await fetchRandomNumbers();
            sum += integerValue1;
        }
        console.log(sum);
    } catch (error) {
        console.error(error);
    }
}
task4();




/*  By Promises
//task-->1
fetchRandomNumbers()
.then((response)=>{
    console.log(response)
})
.catch((error)=>console.error(error))

fetchRandomString()
.then((randomStr) => console.log(randomStr))
.catch((error)=>console.error(error))


//task-->2
fetchRandomNumbers()
    .then((response) => {
        let sum = response;
        console.log(sum);
        return sum;
    })
    .then((sum) => {
        fetchRandomNumbers()
            .then((response1) => {
                sum += response1;
                console.log(sum)
            })
    })
    .catch((error) => console.error(error))


//task-->3
fetchRandomNumbers()
    .then((integerValue) => {
        fetchRandomString()
            .then((stringValue) => {
                let resultValue = integerValue + stringValue;
                console.log(resultValue)
            })
    })
    .catch((error) => console.error(error))


//task-->4

fetchRandomNumbers()
    .then((integerValue) => {
        let sum = integerValue;
        for (let index = 0; index < 10; index++) {
            fetchRandomNumbers()
            .then((integerValue) => {
                sum += integerValue;
            })
        }
        return sum;
    })
    .then((result)=>console.log(result))
    .catch((error) => console.error(error))

*/
